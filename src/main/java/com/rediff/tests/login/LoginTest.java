package com.rediff.tests.login;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rediff.common.Common;
import com.rediff.common.ExcelReader;

public class LoginTest extends Common {
	WebDriver driver = null;
	ExcelReader excel = new ExcelReader();
	@BeforeMethod
	public void OpenBrowser()
	{
<<<<<<< HEAD
          //comments
=======
	    
>>>>>>> 5d54ad7b5241805a5d995fe8cde3651371baf242
		String path = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", path+ "\\src\\main\\resources\\drivers\\windows\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("https://www.rediff.com/");
		log.debug("open the browser and navigated to page");
		
	}
    @Test(priority=1)
	public void verifyPageTitle() 
	{
    	
		String actualPageTitle = driver.getTitle();
		String expectedPageTitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
		Assert.assertEquals(actualPageTitle, expectedPageTitle);
		log.debug("verified page title");
		
	}
	
	
	@Test(priority = 2,dependsOnMethods="verifyPageTitle",enabled=true,dataProvider= "data")
	
	public void verifyInvalidLogin(String un,String ps) throws InterruptedException 
	{
		driver.findElement(By.linkText("Sign in")).click();
		driver.findElement(By.id("login1")).sendKeys(un);
		driver.findElement(By.id("password")).sendKeys(ps);
		driver.findElement(By.cssSelector(".signinbtn")).click();
		Thread.sleep(1500);
		String actualErrorMessage = driver.findElement(By.id("div_login_error")).getText();
		String expectedErrorMessage = "Wrong username and password combination.";
		Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
		log.debug("verified invalid login");
	}
	
	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException
	{
		return excel.readExcelData();
		
	}
	
   @AfterMethod
   public void closeBrowser()
   {
	   driver.close();
	   log.debug("close the browser");
   }

}
