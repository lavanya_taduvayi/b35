package com.rediff.tests.createaccount;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rediff.common.Common;

public class CreateAccount extends Common {
	WebDriver driver=null;
	//comment1
	@BeforeMethod
	public void openBrowser()
	{
		String path = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver",path+ "src\\main\\resources\\drivers\\windows\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.rediff.com/");
		log.debug("open the browser");
	}
	@Test(priority=1, enabled=true)
	public void VerifyUserNameAvailability() throws InterruptedException
	{
		driver.findElement(By.linkText("Create Account")).click();
		driver.findElement(By.xpath("//td[contains(text(),'Full Name')]/parent::tr/td[3]/input")).sendKeys("lavanya");
		driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input")).sendKeys("lavanya");
		driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[2]")).click();
		Thread.sleep(1500);
		String actualError = driver.findElement(By.xpath("//div[@id='check_availability']/font/b")).getText();
		String expectedError = "Sorry, the ID that you are looking for is taken.";
		Assert.assertEquals(actualError, expectedError);
		log.debug("verified username");
	}
	@AfterMethod
	public void closeBrowser()
	{
	 driver.close();
     log.debug("close the browser");
	}

}
